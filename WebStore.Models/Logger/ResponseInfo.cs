﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace WebStore.Models.Logger
{
    public class ResponseInfo
    {
        public DateTime ResponseTime { get; set; }

        public string RequestUrl { get; set; }

        public string UserName { get; set; }

        public StringBuilder Headers { get; set; }

        public string Body { get; set; }

        public string QueryString { get; set; }

        public string StatusCode { get; set; }

        public ResponseInfo()
        {
            Headers = new StringBuilder();
            ResponseTime = DateTime.Now;
        }

        public ResponseInfo(HttpContext context) : this()
        {
            DateTime responseTime = DateTime.Now;
            RequestUrl = context.Request.GetDisplayUrl();

            // get headers
            IHeaderDictionary headersFromRequest = context.Request.Headers;

            if (headersFromRequest.Count > 0)
            {
                foreach (var key in headersFromRequest.Keys)
                {
                    Headers.AppendLine($"{key}: {headersFromRequest[key]}");
                }
            }

            // get body
            //Stream stream = context.Request.InputStream;
            //stream.Seek(0, SeekOrigin.Begin);
            //Body = new StreamReader(stream).ReadToEnd();

            QueryString = context.Request.QueryString.ToString();
            StatusCode = context.Response.StatusCode.ToString();

            UserName = String.Empty;
            if (context.User.Identity.IsAuthenticated)
            {
                UserName = context.User.Identity.Name;
            }
        }
    }
}
