﻿using Microsoft.Extensions.Logging;
using System;

namespace WebStore.Models.Logger
{
    public static class FileLoggerExtensions
    {
        public static ILoggerFactory AddFile(this ILoggerFactory loggerFactory, string filePath)
        {
            loggerFactory.AddProvider(new FileLoggerProvider(filePath));
            return loggerFactory;
        }
    }
}
