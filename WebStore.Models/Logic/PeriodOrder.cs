﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebStore.Models.Logic
{
    public class PeriodOrder
    {
        public DateTime Date { get; set; }

        public string UserEmail { get; set; }

        public int ProductsCount { get; set; }

        public double Price { get; set; }
    }
}
