﻿using System;
using System.Collections.Generic;
using System.Text;
using WebStore.Models.Entities;

namespace WebStore.Models.Logic
{
    public class UserOrderInfo
    {
        public int Id { get; set; }

        public DateTime DateBought { get; set; }

        public List<ProductAmmount> ProductsAmmount { get; set; }

        public double OrderPrice { get; set; }

        public string Username { get; set; }

        public UserOrderInfo()
        {
            ProductsAmmount = new List<ProductAmmount>();
        }

        public UserOrderInfo(Order order) : this()
        {
            Id = order.Id;
            DateBought = order.DateBought;

            foreach (var item in order.OrderProducts)
            {
                var productAmmount = new ProductAmmount(item.Product);
                productAmmount.Ammount = item.ProductAmmount;

                ProductsAmmount.Add(productAmmount);
            }
        }
    }
}
