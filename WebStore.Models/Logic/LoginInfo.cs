﻿using System;

namespace WebStore.Models.Logic
{
    public class LoginInfo
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public bool IsRemembered { get; set; }
    }
}
