﻿using System;
using WebStore.Models.Entities;

namespace WebStore.Models.Logic
{
    public class OrderInfo
    {
        public int Id { get; set; }

        public DateTime DateBought { get; set; }

        public double Price { get; set; }

        public OrderInfo()
        { }

        public OrderInfo(Order order)
        {
            Id = order.Id;
            DateBought = order.DateBought;
        }
    }
}
