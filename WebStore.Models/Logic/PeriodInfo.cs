﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WebStore.Models.Logic
{
    public class PeriodInfo
    {
        public PeriodInfo()
        {
            Orders = new List<PeriodOrder>();
        }

        [Display(Name = "Sales")]
        public int SalesCount { get; set; }

        [Display(Name = "Total price")]
        public double TotalPrice { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public List<PeriodOrder> Orders { get; set; }
    }
}
