﻿using System;
using System.Collections.Generic;
using System.Text;
using WebStore.Models.Entities;

namespace WebStore.Models.Logic
{
    public class ProductInfo
    {
        public int Id { get; set; }
 
        public string Name { get; set; }

        public double Price { get; set; }

        public string Description { get; set; }

        public DateTime DateAdded { get; set; }

        public List<FileModel> Files { get; set; }
    }
}
