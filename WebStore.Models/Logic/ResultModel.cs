﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebStore.Models.Logic
{
    public class ResultModel
    {
        public bool IsSuccess { get; set; }

        public string Message { get; set; }

        public object Object { get; set; }
    }
}
