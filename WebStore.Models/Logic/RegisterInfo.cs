﻿using System;
using WebStore.Models.Entities;

namespace WebStore.Models.Logic
{
    public class RegisterInfo
    {
        public ApplicationUser User { get; set; }

        public string Password { get; set; }

        public string HostName { get; set; }
    }
}
