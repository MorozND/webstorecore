﻿using System;

namespace WebStore.Models.Logic
{
    public static class RoleName
    {
        public const string Admin = "Admin";

        public const string Manager = "Manager";
    }
}
