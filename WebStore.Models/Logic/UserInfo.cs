﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebStore.Models.Logic
{
    public class UserInfo
    {
        public string Id { get; set; }

        [Display(Name = "Full name")]
        public string FullName { get; set; }

        public string Email { get; set; }

        [Display(Name = "Registration date")]
        public DateTime DateRegistered { get; set; }

        public bool IsManager { get; set; }

        public string FileName { get; set; }
    }
}
