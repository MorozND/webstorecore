﻿using System;
using System.Collections.Generic;
using System.Text;
using WebStore.Models.Entities;

namespace WebStore.Models.Logic
{
    public class ProductAmmount
    {
        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public int Ammount { get; set; }

        public ProductAmmount()
        { }

        public ProductAmmount(Product product)
        {
            ProductId = product.Id;
            ProductName = product.Name;
        } 
    }
}
