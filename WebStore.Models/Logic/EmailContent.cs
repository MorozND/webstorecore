﻿using System;
using System.Collections.Generic;

namespace WebStore.Models.Logic
{
    public class EmailContent
    {
        public EmailContent()
        {
            FileNames = new List<string>();
        }

        public string Email { get; set; }

        public string Subject { get; set; }

        public string Message { get; set; }

        public List<string> FileNames { get; set; }
    }
}
