﻿using System;

namespace WebStore.Models.Logic
{
    public static class ResultMessage
    {
        public static readonly string NotFound = "Object not found";
    }
}
