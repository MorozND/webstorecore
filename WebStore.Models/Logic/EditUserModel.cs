﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebStore.Models.Logic
{
    public class EditUserModel
    {
        public string Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        [MaxLength(50)]
        public string Surname { get; set; }
    }
}
