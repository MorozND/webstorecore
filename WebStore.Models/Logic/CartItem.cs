﻿using System;
using WebStore.Models.Entities;

namespace WebStore.Models.Logic
{
    public class CartItem
    {
        public Product Product { get; set; }

        public int Quantity { get; set; }
    }
}
