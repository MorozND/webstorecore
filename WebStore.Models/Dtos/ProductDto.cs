﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace WebStore.Models.Dtos
{
    public class ProductDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public double? Price { get; set; }

        public string Description { get; set; }

        public DateTime? DateAdded { get; set; }

        public List<IFormFile> Files { get; set; }
    }
}
