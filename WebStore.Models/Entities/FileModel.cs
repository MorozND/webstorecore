﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebStore.Models.Entities
{
    public class FileModel
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(150)]
        public string Path { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        public int ProductId { get; set; }

        // navigation properties
        public Product Product { get; set; }
    }
}
