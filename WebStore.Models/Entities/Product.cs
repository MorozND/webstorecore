﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebStore.Models.Entities
{
    public class Product
    {
        public Product()
        {
            OrderProducts = new List<OrderProducts>();
            Files = new List<FileModel>();
        }

        public int Id { get; set; }

        [MaxLength(255)]
        public string Name { get; set; }

        public double Price { get; set; }

        [MaxLength(1024)]
        public string Description { get; set; }

        public DateTime DateAdded { get; set; }

        //navigation properties
        public List<OrderProducts> OrderProducts { get; set; }

        public List<FileModel> Files { get; set; }
    }
}
