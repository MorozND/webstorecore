﻿using System;
using System.Collections.Generic;

namespace WebStore.Models.Entities
{
    public class Order
    {
        public Order()
        {
            OrderProducts = new List<OrderProducts>();
        }

        public int Id { get; set; }

        public DateTime DateBought { get; set; }

        public string UserId { get; set; }

        // navigation properties
        public ApplicationUser User { get; set; }
        public List<OrderProducts> OrderProducts { get; set; }
    }
}
