﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebStore.Models.Entities
{
    public class OrderProducts
    {
        public int Id { get; set; }

        [ForeignKey("Order")]
        public int OrderId { get; set; }
        
        [ForeignKey("Product")]
        public int ProductId { get; set; }

        public int ProductAmmount { get; set; }

        // navigation properties
        public Order Order { get; set; }

        public Product Product { get; set; }
    }
}
