﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Text;
using WebStore.Models.Logger;

namespace WebStore.WEB.Filters
{
    public class RouteLogAttribute : IActionFilter
    {
        private readonly ILogger _logger;

        public RouteLogAttribute(ILogger<RouteLogAttribute> logger)
        {
            _logger = logger;
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var requestInfo = new RequestInfo(context.HttpContext);

            var message = new StringBuilder("\t\tREQUEST INFO\n")
                .AppendLine($"Request Time: {requestInfo.RequestTime}")
                .AppendLine($"Request Url: {requestInfo.RequestUrl}")
                .AppendLine($"Query String: {requestInfo.QueryString}")
                .AppendLine($"HTTP Verb: {requestInfo.HttpVerb}")
                .AppendLine($"Headers: \n{requestInfo.Headers.ToString()}")
                .AppendLine($"Username: {requestInfo.UserName}")
                .AppendLine();

            _logger.LogInformation(message.ToString());
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            var responseInfo = new ResponseInfo(context.HttpContext);

            var message = new StringBuilder("\t\tRESPONSE INFO\n")
                .AppendLine($"Request Time: {responseInfo.ResponseTime}")
                .AppendLine($"Request Url: {responseInfo.RequestUrl}")
                .AppendLine($"Query String: {responseInfo.QueryString}")
                .AppendLine($"Status Code: {responseInfo.StatusCode}")
                .AppendLine($"Headers: \n{responseInfo.Headers.ToString()}")
                .AppendLine($"Username: {responseInfo.UserName}")
                .AppendLine();

            _logger.LogInformation(message.ToString());
        }
    }
}
