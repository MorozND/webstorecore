﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebStore.WEB.Filters
{
    public class ExceptionLogAttribute : IExceptionFilter
    {
        private readonly ILogger _logger;

        public ExceptionLogAttribute(ILogger<ExceptionLogAttribute> logger)
        {
            _logger = logger;
        }

        public void OnException(ExceptionContext context)
        {
            _logger.LogError(context.Exception.Message);
        }
    }
}
