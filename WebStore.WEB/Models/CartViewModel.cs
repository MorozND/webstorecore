﻿using System;
using System.Collections.Generic;
using WebStore.Models.Logic;

namespace WebStore.WEB.Models
{
    public class CartViewModel
    {
        public List<CartItem> Items { get; set; }

        public double TotalPrice { get; set; }
    }
}
