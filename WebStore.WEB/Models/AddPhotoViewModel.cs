﻿using Microsoft.AspNetCore.Http;
using System;

namespace WebStore.WEB.Models
{
    public class AddPhotoViewModel
    {
        public string Id { get; set; }

        public IFormFile File { get; set; }
    }
}
