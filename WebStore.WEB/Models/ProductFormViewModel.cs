﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WebStore.Models.Entities;

namespace WebStore.WEB.Models
{
    public class ProductFormViewModel
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(255)]
        public string Name { get; set; }

        [Required]
        public double? Price { get; set; }

        [MaxLength(1024)]
        public string Description { get; set; }

        public DateTime DateAdded { get; set; }

        public List<IFormFile> Files { get; set; }

        public string Title { get; set; }

        public ProductFormViewModel()
        {
            Id = 0;
            Title = "Add product";
            Files = new List<IFormFile>();
        }

        public ProductFormViewModel(Product product)
        {
            Id = product.Id;
            Name = product.Name;
            Price = product.Price;
            Description = product.Description;
            DateAdded = product.DateAdded;
            Files = new List<IFormFile>();
            Title = "Edit product";
        }
    }
}
