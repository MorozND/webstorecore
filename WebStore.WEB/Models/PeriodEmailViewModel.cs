﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebStore.WEB.Models
{
    public class PeriodEmailViewModel
    {
        [MaxLength(255)]
        [EmailAddress(ErrorMessage = "Please, provie a valid Email address")]
        public string Email { get; set; }

        public string FilePath { get; set; }

        public string FileName { get; set; }
    }
}
