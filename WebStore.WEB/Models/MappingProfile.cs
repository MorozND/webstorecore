﻿using AutoMapper;
using System;
using WebStore.Models.Dtos;
using WebStore.Models.Entities;

namespace WebStore.WEB.Models
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<RegisterViewModel, ApplicationUser>();

            // for update functionality
            CreateMap<Product, Product>()
                .ForMember(dest => dest.Id, opt => opt.Ignore());

            // DTOs
            CreateMap<ProductFormViewModel, ProductDto>();
            CreateMap<ProductDto, Product>()
                .ForMember(dest => dest.DateAdded, opt => opt.MapFrom(
                    src => src.DateAdded ?? DateTime.Now))
                .ForMember(src => src.Files, opt => opt.Ignore());
        }
    }
}
