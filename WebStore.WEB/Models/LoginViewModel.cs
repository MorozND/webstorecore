﻿using System.ComponentModel.DataAnnotations;

namespace WebStore.WEB.Models
{
    public class LoginViewModel
    {
        [Required]
        [MaxLength(255)]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        [Display(Name = "Rememeber me?")]
        public bool RememberMe { get; set; }
    }
}
