﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebStore.BLL.Interfaces;
using WebStore.Models.Dtos;
using WebStore.Models.Entities;
using WebStore.Models.Logic;
using WebStore.WEB.Attributes;
using WebStore.WEB.Models;

namespace WebStore.WEB.Controllers
{
    [Authorize]
    public class ProductsController : Controller
    {
        private IProductService _productService { get; set; }
        private readonly IMapper _mapper;

        public ProductsController(IProductService productService, IMapper mapper)
        {
            _productService = productService;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            return View(_productService.GetAll());
        }

        public IActionResult GetAllPArtial()
        {
            return PartialView(_productService.GetAll());
        }

        [AuthorizeRoles(RoleName.Admin, RoleName.Manager)]
        public IActionResult Add()
        {
            return View("ProductForm", new ProductFormViewModel());
        }

        [AuthorizeRoles(RoleName.Admin, RoleName.Manager)]
        public IActionResult Edit(int id)
        {
            var productFromDb = _productService.Get(id);
            if (productFromDb == null)
                return StatusCode(404);

            return View("ProductForm", new ProductFormViewModel(productFromDb));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Save(ProductFormViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return View("ProductForm", viewModel);            

            if (viewModel.Id == 0) // ADD
            {
                viewModel.DateAdded = DateTime.Now;
                ResultModel result = await _productService.Add(_mapper.Map<ProductDto>(viewModel));

                if (!result.IsSuccess)
                {
                    ModelState.AddModelError("", result.Message);
                    return View("ProductForm", viewModel);
                }
            }
            else // EDIT
            {
                _productService.Update(_mapper.Map<ProductDto>(viewModel));
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [Authorize(Roles = RoleName.Admin)]
        public IActionResult Remove(int id)
        {
            var result = _productService.Remove(id);

            return Json(new { success = result.IsSuccess, message = result.Message });
        }

        public IActionResult ProductInfo(int id)
        {
            var model = _productService.GetProductWithFiles(id);

            if (model == null)
                return StatusCode(404);

            return View(model);
        }
    }
}