﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebStore.BLL.Interfaces;
using WebStore.Models.Logic;
using WebStore.WEB.Models;

namespace WebStore.WEB.Controllers
{
    [Authorize]
    public class OrdersController : Controller
    {
        private readonly IOrderService _orderService;

        public OrdersController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        public async Task<IActionResult> Index()
        {
            var model = await _orderService.GetAllAsync(HttpContext.User);

            if (model == null || model.Count == 0)
                ViewBag.Message = "You don't have any orders yet";

            return View(model);
        }

        public IActionResult Add()
        {
            var result = _orderService.Add(HttpContext);

            if (!result.IsSuccess)
                return View("Error");

            return RedirectToAction("Index", "Products");
        }

        public PartialViewResult GetUserOrderInfo(int id)
        {
            var model = _orderService.GetUserOrderInfo(id);

            if (model == null)
                ViewBag.Message = "Something went wrong";

            return PartialView("_UserOrderInfo", model);
        }

        [Authorize(Roles = RoleName.Admin)]
        public IActionResult Period()
        {
            return View(new PeriodViewModel());
        }

        public async Task<IActionResult> GetPeriodInfo(PeriodViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return View("Period", viewModel);

            var model = await _orderService.GetPeriodInfo(viewModel.StartDate, viewModel.EndDate);

            if (model == null)
                ViewBag.Message = "There are no orders in the specified period";

            return PartialView("_PeriodInfo", model);
        }

        [HttpPost]
        public async Task<IActionResult> ExportToExcel(PeriodInfo viewModel)
        {
            Tuple<string, string> file = _orderService.ExportToExcel(viewModel);

            var memory = new MemoryStream();
            using (var stream = new FileStream(Path.Combine(file.Item1, file.Item2), FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;

            return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", file.Item2);
        }
        
        [HttpPost]
        public PartialViewResult SendByEmail(PeriodInfo viewModel)
        {
            Tuple<string, string> file = _orderService.ExportToExcel(viewModel);

            var model = new PeriodEmailViewModel()
            {
                FilePath = file.Item1,
                FileName = file.Item2
            };

            return PartialView("_PeriodEmail", model);
        }

        [HttpPost]
        public async Task<PartialViewResult> SendEmail(PeriodEmailViewModel viewModel)
        {
            var emailContent = new EmailContent()
            {
                Email = viewModel.Email,
                Subject = "Orders info",
                Message = "In the attached file you can find all the neccessary info about orders in the selected period"
            };

            emailContent.FileNames.Add(Path.Combine(viewModel.FilePath, viewModel.FileName));

            var result = await _orderService.SendByEmailAsync(emailContent);

            if (result.IsSuccess)
                ViewBag.Success = result.Message;
            else
                ViewBag.Error = result.Message;

            return PartialView("_PeriodEmail", viewModel);
        }
    }
}