﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebStore.BLL.Interfaces;
using WebStore.Models.Logic;
using WebStore.WEB.Models;

namespace WebStore.WEB.Controllers
{
    [Authorize]
    public class UsersController : Controller
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [Authorize(Roles = RoleName.Admin)]
        public async Task<IActionResult> Index()
        {
            var model = await _userService.GetAsync();

            if (model == null || model.Count == 0)
                ViewBag.Message = "There are no any users yet :(";

            return View(model);
        }

        [Authorize(Roles = RoleName.Admin)]
        public async Task<IActionResult> PromoteToManager(string id)
        {
            var result = await _userService.PromoteToManagerAsync(id);

            if (!result.IsSuccess)
            {
                TempData["Error"] = result.Message;
            }

            return RedirectToAction("Index");
        }

        [Authorize(Roles = RoleName.Admin)]
        public async Task<IActionResult> DowngradeFromManager(string id)
        {
            var result = await _userService.DowngradeFromManagerAsync(id);

            if (!result.IsSuccess)
            {
                TempData["Error"] = result.Message;
            }

            return RedirectToAction("Index");
        }
        
        public async Task<IActionResult> UserProfile(string id)
        {
            var userProfile = await _userService.GetUserInfoAsync(id);
            if (userProfile == null)
                return StatusCode(404);

            ViewBag.Error = TempData["EditError"];

            return View(userProfile);
        }

        [HttpGet]
        public IActionResult AddPhoto(string id)
        {
            return PartialView("_AddPhoto", new AddPhotoViewModel() { Id = id });
        }

        [HttpPost]
        public async Task<IActionResult> AddPhoto(AddPhotoViewModel viewModel)
        {
            var result = await _userService.AddPhotoAsync(viewModel.Id, viewModel.File);

            return RedirectToAction("UserProfile", new { id = viewModel.Id });
        }

        public async Task<IActionResult> DeletePhoto(string id)
        {
            await _userService.DeletePhotoAsync(id);

            return RedirectToAction("UserProfile", new { id = id });
        }

        public async Task<PartialViewResult> EditUser(string id)
        {
            var model = await _userService.GetEditUserModel(id);

            if (model == null)
                ViewBag.Error = "There is no User with the given Id";

            return PartialView("_EditUser", model);
        }

        [HttpPost]
        public async Task<IActionResult> EditUser(EditUserModel model)
        {
            if (!ModelState.IsValid)
            {
                TempData["EditError"] = "Please, enter correct values for editting the user";
                return RedirectToAction(nameof(UserProfile), new { id = model.Id });
            }

            var result = await _userService.EditUser(model);
            if (!result.IsSuccess)
            {
                TempData["EditError"] = "There was an error while editting user profile";
            }

            return RedirectToAction(nameof(UserProfile), new { id = model.Id });
        }
    }
}