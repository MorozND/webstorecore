﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebStore.BLL.Interfaces;
using WebStore.Models.Entities;
using WebStore.Models.Logic;
using WebStore.WEB.Models;

namespace WebStore.WEB.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAccountService _accountService;
        private readonly IMapper _mapper;

        public AccountController(IAccountService accountService, IMapper mapper)
        {
            _accountService = accountService;
            _mapper = mapper;
        }

        public IActionResult Register()
        {
            return View(new RegisterViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return View(viewModel);

            var user = _mapper.Map<ApplicationUser>(viewModel);
            user.DateRegistered = DateTime.Now;
            user.UserName = viewModel.Email;

            var regInfo = new RegisterInfo
            {
                User = user,
                Password = viewModel.Password,
                HostName = Request.Host.Value
            };

            ResultModel result = await _accountService.Register(regInfo);

            if (result.IsSuccess)
            {
                return Content(result.Message);
            }
            else
            {
                ModelState.AddModelError("", result.Message);
                return View(viewModel);
            }
        }

        public IActionResult Login()
        {
            return View(new LoginViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return View(viewModel);

            var loginInfo = new LoginInfo
            {
                Email = viewModel.Email,
                Password = viewModel.Password,
                IsRemembered = viewModel.RememberMe
            };

            ResultModel result = await _accountService.Login(loginInfo);

            if (result.IsSuccess)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("", result.Message);
                return View(viewModel);
            }
        }

        [HttpPost]
        public async Task<IActionResult> LogOff()
        {
            await _accountService.LogOff();
            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string token)
        {
            if (userId == null || token == null)
                return View("Error");

            ResultModel result = await _accountService.ConfirmEmail(userId, token);

            if (result.IsSuccess)
                return RedirectToAction("Index", "Home");
            else
                return View("Error");
        }

        [HttpPost]
        public IActionResult SignInWithGoogle()
        {
            return Challenge(_accountService.SignInWithGoogle(), "Google");
        }

        [HttpPost]
        public IActionResult SignInWithFacebook()
        {
            return Challenge(_accountService.SignInWithFacebook(), "Facebook");
        }

        [AllowAnonymous]
        public async Task<IActionResult> HandleExternalLogin()
        {
            var result = await _accountService.HandleExternalLoginAsync(HttpContext);

            if (!result.IsSuccess)
                return View("Error");

            return RedirectToAction("Index", "Home");
        }
    }
}