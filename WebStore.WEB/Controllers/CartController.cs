﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebStore.BLL.Interfaces;
using WebStore.Models.Logic;
using WebStore.WEB.Models;

namespace WebStore.WEB.Controllers
{
    [Authorize]
    public class CartController : Controller
    {
        private ICartService _cartService;

        public CartController(ICartService cartService)
        {
            _cartService = cartService;
        }

        public IActionResult Index()
        {
            var viewModel = new CartViewModel()
            {
                Items = _cartService.GetCart(HttpContext)
            };

            viewModel.TotalPrice = (viewModel.Items == null) ? 0 : GetTotalPrice(viewModel.Items);

            if (viewModel.Items == null || viewModel.Items.Count == 0)
                ViewBag.Message = "You cart is empty";

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Add(int id)
        {
            var result = _cartService.Add(id, HttpContext);

            if (!result.IsSuccess)
            {
                if (result.Message == ResultMessage.NotFound)
                    return StatusCode(404);
                else
                    return View("Error");
            }

            return RedirectToAction("GetAllPartial", "Products");
        }

        public IActionResult Remove(int id)
        {
            var result = _cartService.Remove(id, HttpContext);

            if (!result.IsSuccess)
                return View("Error");

            return RedirectToAction("Index");
        }

        public IActionResult AddSingle(int id)
        {
            var result = _cartService.AddSingle(id, HttpContext);

            if (!result.IsSuccess)
                return View("Error");

            return RedirectToAction("Index");
        }

        public IActionResult RemoveSingle(int id)
        {
            var result = _cartService.RemoveSingle(id, HttpContext);

            if (!result.IsSuccess)
                return View("Error");

            return RedirectToAction("Index");
        }

        public IActionResult EmptyCart()
        {
            _cartService.EmptyCart(HttpContext);

            return RedirectToAction("Index");
        }

        #region Private helpers
        private double GetTotalPrice(IEnumerable<CartItem> items)
        {
            double total = 0;

            foreach (var item in items)
            {
                total += (item.Product.Price * item.Quantity);
            }

            return total;
        }
        #endregion
    }
}