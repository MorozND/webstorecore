﻿using Microsoft.AspNetCore.Authorization;
using System;

namespace WebStore.WEB.Attributes
{
    public class AuthorizeRolesAttribute : AuthorizeAttribute
    {
        public AuthorizeRolesAttribute(params string[] roles)
            : base()
        {
            Roles = String.Join(",", roles);
        }
    }
}
