﻿using System.Linq;
using System.Collections.Generic;
using WebStore.DAL.Context;
using WebStore.DAL.Interfaces;
using WebStore.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace WebStore.DAL.Repositories
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository(ApplicationContext context)
            : base(context)
        { }

        public List<OrderProducts> GetAllOrderProducts()
        {
            return _context.Set<OrderProducts>().ToList();
        }

        public Product GetProductWithFiles(int id)
        {
            return _context.Set<Product>()
                        .Include(p => p.Files)
                        .SingleOrDefault(p => p.Id == id);
        }
    }
}
