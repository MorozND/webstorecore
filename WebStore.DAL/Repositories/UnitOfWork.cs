﻿
using Microsoft.EntityFrameworkCore;
using WebStore.DAL.Context;
using WebStore.DAL.Interfaces;
using WebStore.Models.Entities;

namespace WebStore.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext _context;

        public UnitOfWork(ApplicationContext context)
        {
            _context = context;
            Products = new ProductRepository(context);
            Orders = new OrderRepository(context);
            Files = new FileModelRepository(context);
        }

        public IProductRepository Products { get; private set; }
        public IOrderRepository Orders { get; private set; }
        public IFileModelRepository Files { get; set; }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
