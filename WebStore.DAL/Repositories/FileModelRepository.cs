﻿using System;
using WebStore.DAL.Context;
using WebStore.DAL.Interfaces;
using WebStore.Models.Entities;

namespace WebStore.DAL.Repositories
{
    public class FileModelRepository : Repository<FileModel>, IFileModelRepository
    {
        public FileModelRepository(ApplicationContext context)
            : base(context)
        { }
    }
}
