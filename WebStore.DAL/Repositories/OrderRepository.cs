﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using WebStore.DAL.Context;
using WebStore.DAL.Interfaces;
using WebStore.Models.Entities;

namespace WebStore.DAL.Repositories
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(ApplicationContext context)
            : base(context)
        { }

        public List<Order> GetAllOrdersWithOrderProducts()
        {
            return _context.Set<Order>()
                .Include(o => o.OrderProducts).ThenInclude(op => op.Product)
                .ToList();
        }

        public Order GetOrderWithRelatedObjects(int id)
        {
            return _context.Set<Order>()
                .Include(o => o.OrderProducts).ThenInclude(op => op.Product)
                .Include(o => o.User)
                .SingleOrDefault(o => o.Id == id);
        }
    }
}
