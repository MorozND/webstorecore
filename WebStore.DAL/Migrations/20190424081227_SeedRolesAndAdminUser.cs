﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebStore.DAL.Migrations
{
    public partial class SeedRolesAndAdminUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Name], [Surname], [DateRegistered]) VALUES (N'f822260e-ef2d-4347-8437-b1084cdb3e94', N'nikitamoroz118@gmail.com', N'NIKITAMOROZ118@GMAIL.COM', N'nikitamoroz118@gmail.com', N'NIKITAMOROZ118@GMAIL.COM', 1, N'AQAAAAEAACcQAAAAEIPqY/kbo3DG0QybLJzZcFYZo6+N/fBOm778FsAfUSCUxYEaizOIWKxPwercdaAa6Q==', N'ZEYHVRV3VE3O77ADMQFQS4R7SEYPSOQB', N'04cb296f-2a1e-477b-809d-1618b46423a7', NULL, 0, 0, NULL, 1, 0, N'Nikita', N'Moroz', N'2019-04-24 10:13:52')

                INSERT INTO [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'092156cb-a39f-424b-8fee-b7e684f4025a', N'Manager', N'MANAGER', N'd7d90677-b32f-4160-a53b-f482dde4c356')
                INSERT INTO [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'51866dac-5b7c-4e71-abec-3a2d6ff46b10', N'Admin', N'ADMIN', N'203acc6f-81ea-4c04-a00b-a94e545ee66b')

                INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'f822260e-ef2d-4347-8437-b1084cdb3e94', N'51866dac-5b7c-4e71-abec-3a2d6ff46b10')
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
