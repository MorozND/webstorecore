﻿using System.Collections.Generic;
using WebStore.Models.Entities;

namespace WebStore.DAL.Interfaces
{
    public interface IProductRepository : IRepository<Product>
    {
        List<OrderProducts> GetAllOrderProducts();
        Product GetProductWithFiles(int id);
    }
}
