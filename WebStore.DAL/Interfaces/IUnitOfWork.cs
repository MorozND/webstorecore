﻿using System;

namespace WebStore.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IProductRepository Products { get; }
        IOrderRepository Orders { get; }
        IFileModelRepository Files { get;  }

        int Complete();
    }
}
