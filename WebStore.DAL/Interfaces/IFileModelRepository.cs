﻿using System;
using WebStore.Models.Entities;

namespace WebStore.DAL.Interfaces
{
    public interface IFileModelRepository : IRepository<FileModel>
    {
    }
}
