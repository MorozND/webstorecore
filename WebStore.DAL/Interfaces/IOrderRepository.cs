﻿using System;
using System.Collections.Generic;
using WebStore.Models.Entities;

namespace WebStore.DAL.Interfaces
{
    public interface IOrderRepository : IRepository<Order>
    {
        List<Order> GetAllOrdersWithOrderProducts();
        Order GetOrderWithRelatedObjects(int id);
    }
}
