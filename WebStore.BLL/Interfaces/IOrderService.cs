﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;
using WebStore.Models.Logic;

namespace WebStore.BLL.Interfaces
{
    public interface IOrderService
    {
        Task<List<OrderInfo>> GetAllAsync(ClaimsPrincipal user);
        ResultModel Add(HttpContext context);
        UserOrderInfo GetUserOrderInfo(int orderId);
        Task<PeriodInfo> GetPeriodInfo(DateTime startDate, DateTime endDate);
        Tuple<string, string> ExportToExcel(PeriodInfo periodInfo);
        Task<ResultModel> SendByEmailAsync(EmailContent emailContent);
    }
}
