﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using WebStore.Models.Logic;

namespace WebStore.BLL.Interfaces
{
    public interface ICartService
    {
        List<CartItem> GetCart(HttpContext context);
        ResultModel Add(int id, HttpContext context);
        ResultModel AddSingle(int id, HttpContext context);
        ResultModel Remove(int id, HttpContext context);
        ResultModel RemoveSingle(int id, HttpContext context);
        void EmptyCart(HttpContext context);
    }
}
