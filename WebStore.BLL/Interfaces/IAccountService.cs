﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using WebStore.Models.Logic;

namespace WebStore.BLL.Interfaces
{
    public interface IAccountService
    {
        Task<ResultModel> Register(RegisterInfo regInfo);
        Task<ResultModel> Login(LoginInfo loginInfo);
        Task<ResultModel> ConfirmEmail(string userId, string token);
        Task LogOff();
        Task<ResultModel> HandleExternalLoginAsync(HttpContext context);
        AuthenticationProperties SignInWithGoogle();
        AuthenticationProperties SignInWithFacebook();
    }
}
