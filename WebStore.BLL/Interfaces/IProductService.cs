﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebStore.Models.Dtos;
using WebStore.Models.Entities;
using WebStore.Models.Logic;

namespace WebStore.BLL.Interfaces
{
    public interface IProductService
    {
        Task<ResultModel> Add(ProductDto product);
        void Update(ProductDto product);
        ResultModel Remove(int id);
        Product Get(int id);
        Product GetProductWithFiles(int id);
        IEnumerable<Product> GetAll();
    }
}
