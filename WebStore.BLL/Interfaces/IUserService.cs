﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebStore.Models.Entities;
using WebStore.Models.Logic;

namespace WebStore.BLL.Interfaces
{
    public interface IUserService
    {
        Task<List<UserInfo>> GetAsync();
        Task<ResultModel> PromoteToManagerAsync(string id);
        Task<ResultModel> DowngradeFromManagerAsync(string id);
        Task<UserInfo> GetUserInfoAsync(string id);
        Task<ResultModel> AddPhotoAsync(string id, IFormFile file);
        Task DeletePhotoAsync(string id);
        Task<EditUserModel> GetEditUserModel(string id);
        Task<ResultModel> EditUser(EditUserModel model);
    }
}
