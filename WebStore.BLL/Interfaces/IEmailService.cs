﻿using System;
using System.Threading.Tasks;
using WebStore.Models.Logic;

namespace WebStore.BLL.Interfaces
{
    public interface IEmailService
    {
        Task SendEmailAsync(EmailContent emailContent);
    }
}
