﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Security.Claims;
using WebStore.BLL.Interfaces;
using WebStore.DAL.Interfaces;
using WebStore.Models.Entities;
using WebStore.Models.Logic;
using Microsoft.AspNetCore.Http;
using WebStore.Models.Helpers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace WebStore.BLL.Services
{
    public class OrderService : IOrderService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IHostingEnvironment _appEnvironment;
        private readonly IEmailService _emailService;
        private readonly ICartService _cartService;

        public OrderService(
            UserManager<ApplicationUser> userManager, 
            IUnitOfWork unitOfWork,
            IHostingEnvironment appenvironment,
            IEmailService emailService,
            ICartService cartService)
        {
            _userManager = userManager;
            _unitOfWork = unitOfWork;
            _appEnvironment = appenvironment;
            _emailService = emailService;
            _cartService = cartService;
        }

        public async Task<List<OrderInfo>> GetAllAsync(ClaimsPrincipal user)
        {
            var result = new List<OrderInfo>();

            var userId = _userManager.GetUserId(user);
            var userFromDb = await _userManager.FindByIdAsync(userId);

            // get all orders if Admin and their own orders if users
            var orders = await _userManager.IsInRoleAsync(userFromDb, RoleName.Admin)
                ? _unitOfWork.Orders.GetAllOrdersWithOrderProducts()
                : _unitOfWork.Orders.GetAllOrdersWithOrderProducts()
                                        .Where(o => o.UserId == userId);

            foreach (var order in orders)
            {
                var orderInfo = new OrderInfo(order)
                {
                    Price = CountOrderPrice(order)
                };

                result.Add(orderInfo);
            }

            return result;
        }

        public ResultModel Add(HttpContext context)
        {
            var result = new ResultModel() { IsSuccess = true };
            var cartItems = _cartService.GetCart(context);

            string userId = _userManager.GetUserId(context.User);
            var userFromDb = _userManager.Users.SingleOrDefault(u => u.Id == userId);

            var order = new Order() { DateBought = DateTime.Now};
            userFromDb.Orders.Add(order);

            foreach (var item in cartItems)
            {
                var productFromDb = _unitOfWork.Products.GetAll().SingleOrDefault(p => p.Id == item.Product.Id);
                if (productFromDb == null)
                {
                    result.IsSuccess = false;
                    result.Message = ResultMessage.NotFound;
                    return result;
                }

                order.OrderProducts.Add(new OrderProducts
                    {
                        Product = productFromDb,
                        ProductAmmount = item.Quantity
                    }
                );
            }

            _unitOfWork.Complete();

            _cartService.EmptyCart(context);

            return result;
        }

        public UserOrderInfo GetUserOrderInfo(int orderId)
        {
            var orderFromDb = _unitOfWork.Orders.GetOrderWithRelatedObjects(orderId);
            if (orderFromDb == null)
                return null;

            var result = new UserOrderInfo(orderFromDb);
            result.OrderPrice = CountOrderPrice(orderFromDb);
            result.Username = (orderFromDb.User != null)
                ? orderFromDb.User.Email
                : "?";

            return result;
        }

        public async Task<PeriodInfo> GetPeriodInfo(DateTime startDate, DateTime endDate)
        {
            var ordersFromDb = _unitOfWork.Orders
                                    .GetAllOrdersWithOrderProducts()
                                    .Where(o => o.DateBought >= startDate && o.DateBought <= endDate)
                                    .ToList();

            if (ordersFromDb == null || ordersFromDb.Count == 0)
                return null;

            var periodInfo = new PeriodInfo()
            {
                SalesCount = ordersFromDb.Count,
                TotalPrice = CountOrdersPrice(ordersFromDb),
                StartDate = startDate,
                EndDate = endDate
            };

            foreach (var order in ordersFromDb)
            {
                var user = await _userManager.FindByIdAsync(order.UserId);
                var periodOrder = new PeriodOrder()
                {
                    Date = order.DateBought,
                    UserEmail = user.Email,
                    Price = CountOrderPrice(order),
                    ProductsCount = CountProductsInOrder(order)
                };

                periodInfo.Orders.Add(periodOrder);
            }

            return periodInfo;
        }

        public Tuple<string, string> ExportToExcel(PeriodInfo periodInfo)
        {
            var webRoot = _appEnvironment.WebRootPath;
            var fileName = @"file1.xlsx";
            var file = new FileInfo(Path.Combine(webRoot, fileName));

            using (var fs = new FileStream(Path.Combine(webRoot, fileName), FileMode.Create, FileAccess.Write))
            {
                IWorkbook workbook = new XSSFWorkbook();
                ISheet excelSheet = workbook.CreateSheet("Period info");

                IRow row = excelSheet.CreateRow(0);
                row.CreateCell(0).SetCellValue("Start date:");
                row.CreateCell(1).SetCellValue(periodInfo.StartDate.ToString("dd.MM.yyyy"));

                row = excelSheet.CreateRow(1);
                row.CreateCell(0).SetCellValue("End date:");
                row.CreateCell(1).SetCellValue(periodInfo.EndDate.ToString("dd.MM.yyyy"));

                row = excelSheet.CreateRow(2);
                row.CreateCell(0).SetCellValue("Sales count:");
                row.CreateCell(1).SetCellValue(periodInfo.SalesCount);

                row = excelSheet.CreateRow(3);
                row.CreateCell(0).SetCellValue("Total price:");
                row.CreateCell(1).SetCellValue(periodInfo.TotalPrice);

                row = excelSheet.CreateRow(4);

                row = excelSheet.CreateRow(5);
                row.CreateCell(0).SetCellValue("Date");
                row.CreateCell(1).SetCellValue("User email");
                row.CreateCell(2).SetCellValue("Products");
                row.CreateCell(3).SetCellValue("Price");

                for (int i = 0; i < periodInfo.Orders.Count; i++)
                {
                    row = excelSheet.CreateRow(i + 6);
                    row.CreateCell(0).SetCellValue(periodInfo.Orders[i].Date.ToString("dd.MM.yyyy"));
                    row.CreateCell(1).SetCellValue(periodInfo.Orders[i].UserEmail);
                    row.CreateCell(2).SetCellValue(periodInfo.Orders[i].ProductsCount);
                    row.CreateCell(3).SetCellValue(periodInfo.Orders[i].Price);
                }

                workbook.Write(fs);
            }

            return new Tuple<string, string>(webRoot, fileName);
        }

        public async Task<ResultModel> SendByEmailAsync(EmailContent emailContent)
        {
            var result = new ResultModel()
            {
                IsSuccess = true,
                Message = "Email has been successfully sent"
            };

            try
            {
                await _emailService.SendEmailAsync(emailContent);
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = "There was an error while sending your email";
            }

            return result;
        }

        #region Private helpers
        private double CountOrderPrice(Order order)
        {
            double result = 0;

            foreach (var productOrder in order.OrderProducts)
            {
                result += (productOrder.Product.Price * productOrder.ProductAmmount);
            }

            return result;
        }

        private double CountOrdersPrice(IEnumerable<Order> orders)
        {
            double result = 0;

            foreach (var order in orders)
            {
                result += CountOrderPrice(order);
            }

            return result;
        }

        private int CountProductsInOrder(Order order)
        {
            int count = 0;

            foreach (var productOrder in order.OrderProducts)
            {
                count += productOrder.ProductAmmount;
            }

            return count;
        }
        #endregion
    }
}
