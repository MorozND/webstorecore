﻿using Microsoft.AspNetCore.Identity;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebStore.BLL.Interfaces;
using WebStore.Models.Logic;
using System.Security.Policy;
using System.Net;
using System;
using WebStore.Models.Interfaces;
using WebStore.Models.Entities;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;

namespace WebStore.BLL.Services
{
    public class AccountService : IAccountService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IEmailService _emailService;

        public AccountService(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            RoleManager<IdentityRole> roleManager,
            IEmailService emailService)

        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _emailService = emailService;
        }

        public async Task<ResultModel> Register(RegisterInfo regInfo)
        {
            IdentityResult identityResult = await _userManager.CreateAsync(regInfo.User, regInfo.Password);

            var result = new ResultModel()
            {
                IsSuccess = identityResult.Succeeded
            };

            if (result.IsSuccess)
            {
                var token = await _userManager.GenerateEmailConfirmationTokenAsync(regInfo.User);
                token = WebUtility.UrlEncode(token);

                // generate confirmation Url
                var confirmationUrl = new StringBuilder("https://")
                    .Append(regInfo.HostName)
                    .Append("/Account/ConfirmEmail")
                    .Append($"?userId={regInfo.User.Id}&token={token}&");

                // send email
                var emailContent = new EmailContent()
                {
                    Email = regInfo.User.Email,
                    Subject = "Confirm registration",
                    Message = $"Confirm your registration following this link: <a href='{confirmationUrl}'>Click me</a>"
                };

                await _emailService.SendEmailAsync(emailContent);

                result.Message = "Confirmation email was sent to your mailbox.";
            }
            else
            {
                // get last error
                IdentityError error = identityResult.Errors.Last();
                result.Message = error.Description;
            }

            return result;
        }

        public async Task<ResultModel> Login(LoginInfo loginInfo)
        {
            var result = new ResultModel();
            var user = await _userManager.FindByEmailAsync(loginInfo.Email);

            if (user != null)
            {
                if (!await _userManager.IsEmailConfirmedAsync(user))
                {
                    result.Message = "You haven't confirmed your Email.";
                    return result;
                }                    

                SignInResult signInResult = 
                    await _signInManager.PasswordSignInAsync(
                        loginInfo.Email, 
                        loginInfo.Password, 
                        loginInfo.IsRemembered, 
                        false
                    );

                result.IsSuccess = signInResult.Succeeded;
                if (result.IsSuccess == false)
                    result.Message = "Invalid email or password";
            }
            else
            {
                result.Message = "User not found";
            }

            return result;     
        }

        public async Task LogOff()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task<ResultModel> ConfirmEmail(string userId, string token)
        {
            var result = new ResultModel();

            var user = await _userManager.FindByIdAsync(userId);

            if (user == null)
                return result;

            var confirmResult = await _userManager.ConfirmEmailAsync(user, token);
            result.IsSuccess = confirmResult.Succeeded;

            return result;
        }

        public async Task<ResultModel> HandleExternalLoginAsync(HttpContext context)
        {
            var result = new ResultModel() { IsSuccess = true };

            var info = await _signInManager.GetExternalLoginInfoAsync();
            var signInResult = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false);

            if (!signInResult.Succeeded) // user doesn't exist yet
            {
                var email = info.Principal.FindFirstValue(ClaimTypes.Email);
                var newUser = new ApplicationUser()
                {
                    UserName = email,
                    Email = email,
                    EmailConfirmed = true,
                    DateRegistered = DateTime.Now,
                    Name = String.Empty,
                    Surname = String.Empty
                };

                var createResult = await _userManager.CreateAsync(newUser);
                if (!createResult.Succeeded)
                {
                    result.IsSuccess = false;
                    result.Message = "Failed to create new User";
                    return result;
                }

                await _userManager.AddLoginAsync(newUser, info);
                var newUserClaims = info.Principal.Claims.Append(new Claim("userId", newUser.Id));
                await _userManager.AddClaimsAsync(newUser, newUserClaims);
                await _signInManager.SignInAsync(newUser, isPersistent: false);
                await context.SignOutAsync(IdentityConstants.ExternalScheme);
            }

            return result;
        }

        public AuthenticationProperties SignInWithGoogle()
        {
            return _signInManager
                .ConfigureExternalAuthenticationProperties("Google", new PathString("/Account/HandleExternalLogin"));
        }

        public AuthenticationProperties SignInWithFacebook()
        {
            return _signInManager
                .ConfigureExternalAuthenticationProperties("Facebook", new PathString("/Account/HandleExternalLogin"));
        }
    }
}
