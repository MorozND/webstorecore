﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebStore.BLL.Interfaces;
using WebStore.DAL.Interfaces;
using WebStore.DAL.Repositories;
using AutoMapper;
using WebStore.Models.Entities;
using WebStore.Models.Logic;
using WebStore.Models.Dtos;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System.Threading.Tasks;

namespace WebStore.BLL.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IHostingEnvironment _appEnvironment;

        private const int TwoMegaBytes = 2 * (1024 * 1024);

        public ProductService(
            IUnitOfWork unitOfWork, 
            IMapper mapper,
            IHostingEnvironment appEnvironment)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _appEnvironment = appEnvironment;
        }

        public async Task<ResultModel> Add(ProductDto productDto)
        {
            var result = new ResultModel() { IsSuccess = true };

            var newProduct = _mapper.Map<Product>(productDto);
            _unitOfWork.Products.Add(newProduct);

            if (productDto.Files.Count > 0)
            {
                foreach (var file in productDto.Files)
                {
                    if (file.Length > TwoMegaBytes)
                    {
                        result.IsSuccess = false;
                        result.Message = "Images could not be greater than 2 megabytes";
                        return result;
                    }

                    string path = $@"{_appEnvironment.WebRootPath}\products\{productDto.Name}\";
                    Directory.CreateDirectory(path);

                    string fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
                    using (var fileStream = new FileStream(path + fileName, FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                    }

                    var fileModel = new FileModel() { Path = path, Name = fileName };
                    newProduct.Files.Add(fileModel);
                }
            }
            else
            {
                result.IsSuccess = false;
                result.Message = "Choose at least one photo for a product";
                return result;
            }

            _unitOfWork.Complete();

            return result;
        }

        public Product Get(int id)
        {
            return _unitOfWork.Products.Get(id);
        }

        public IEnumerable<Product> GetAll()
        {
            return _unitOfWork.Products.GetAll();
        }

        public Product GetProductWithFiles(int id)
        {
            return _unitOfWork.Products.GetProductWithFiles(id);
        }

        public ResultModel Remove(int id)
        {
            var result = new ResultModel() { IsSuccess = true };

            var productFromDb = _unitOfWork.Products.Get(id);

            if (productFromDb == null)
            {
                result.IsSuccess = false;
                result.Message = ResultMessage.NotFound;
                return result;
            }

            var orderProducts = _unitOfWork.Products.GetAllOrderProducts();

            if (orderProducts == null || orderProducts.Count == 0)
            {
                DeleteProduct(productFromDb);
            }
            else
            {
                var idList = orderProducts.Where(op => op.ProductId == id).ToList();

                if (idList.Count == 0)
                {
                    DeleteProduct(productFromDb);
                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = "There exists an order with the product being deleted. You can't delete the product.";
                }
            }

            _unitOfWork.Complete();

            return result;
        }

        public void Update(ProductDto product)
        {
            var productFromDb = _unitOfWork.Products.Get(product.Id);

            if(product.Name != productFromDb.Name)
            {
                string oldName = $@"{_appEnvironment.WebRootPath}\products\{productFromDb.Name}";
                string newName = $@"{_appEnvironment.WebRootPath}\products\{product.Name}";

                Directory.Move(oldName, newName);
            }

            _mapper.Map(product, productFromDb);

            _unitOfWork.Complete();
        }

        #region Private helpers
        private void DeleteProduct(Product product)
        {
            _unitOfWork.Products.Remove(product);

            // delete product directory
            DirectoryInfo di = new DirectoryInfo($@"{_appEnvironment.WebRootPath}\products\{product.Name}");
            foreach (var fileInfo in di.GetFiles())
            {
                fileInfo.Delete();
            }

            di.Delete();
        }
        #endregion
    }
}
