﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using WebStore.BLL.Interfaces;
using WebStore.DAL.Interfaces;
using WebStore.Models.Entities;
using WebStore.Models.Logic;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;

namespace WebStore.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IHostingEnvironment _appEnvironment;

        public UserService(
            UserManager<ApplicationUser> userManager, 
            IUnitOfWork unitOfWork,
            IHostingEnvironment appEnvironment)
        {
            _userManager = userManager;
            _unitOfWork = unitOfWork;
            _appEnvironment = appEnvironment;
        }

        public async Task<List<UserInfo>> GetAsync()
        {
            var result = new List<UserInfo>();

            // get all users except admins
            var admins = await _userManager.GetUsersInRoleAsync(RoleName.Admin);
            var users = _userManager.Users.Where(u => !admins.Contains(u)).ToList();

            foreach (var user in users)
            {
                var userInfo = new UserInfo()
                {
                    Id = user.Id,
                    Email = user.Email,
                    FullName = String.Format($"{user.Name} {user.Surname}"),
                    DateRegistered = user.DateRegistered,
                    IsManager = await _userManager.IsInRoleAsync(user, RoleName.Manager)
                };

                result.Add(userInfo);
            }

            return result;
        }

        public async Task<ResultModel> PromoteToManagerAsync(string id)
        {
            var result = new ResultModel();

            var user = await _userManager.FindByIdAsync(id);

            var identityResult = await _userManager.AddToRoleAsync(user, RoleName.Manager);
            result.IsSuccess = identityResult.Succeeded;

            if (!result.IsSuccess)
            {
                // get last error
                IdentityError error = identityResult.Errors.Last();
                result.Message = error.Description;
            }

            return result;
        }

        public async Task<ResultModel> DowngradeFromManagerAsync(string id)
        {
            var result = new ResultModel();

            var user = await _userManager.FindByIdAsync(id);

            var identityResult = await _userManager.RemoveFromRoleAsync(user, RoleName.Manager);
            result.IsSuccess = identityResult.Succeeded;

            if (!result.IsSuccess)
            {
                // get last error
                IdentityError error = identityResult.Errors.Last();
                result.Message = error.Description;
            }

            return result;
        }

        public async Task<UserInfo> GetUserInfoAsync(string id)
        {
            ApplicationUser user = await _userManager.FindByIdAsync(id);
            if (user == null)
                return null;

            var userInfo = new UserInfo()
            {
                Id = user.Id,
                Email = user.Email,
                FullName = String.Format($"{user.Name} {user.Surname}"),
                DateRegistered = user.DateRegistered,
                IsManager = await _userManager.IsInRoleAsync(user, RoleName.Manager),
                FileName = user.FileName
            };

            return userInfo;
        }

        public async Task<ResultModel> AddPhotoAsync(string id, IFormFile file)
        {
            var result = new ResultModel() { IsSuccess = true };

            if (file == null)
            {
                result.IsSuccess = false;
                result.Message = "Choose a file";
                return result;
            }

            ApplicationUser user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                result.IsSuccess = false;
                result.Message = ResultMessage.NotFound;
            }

            string path = $@"{_appEnvironment.WebRootPath}\users\{user.Email}\";
            Directory.CreateDirectory(path);

            // if directory already existed – clean it
            DirectoryInfo di = new DirectoryInfo(path);
            foreach (var fileInfo in di.GetFiles())
            {
                fileInfo.Delete();
            }

            using (var fileStream = new FileStream(path + file.FileName, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
            }

            user.FileName = file.FileName;

            _unitOfWork.Complete();

            return result;
        }

        public async Task DeletePhotoAsync(string id)
        {
            ApplicationUser user = await _userManager.FindByIdAsync(id);

            user.FileName = null;
            _unitOfWork.Complete();

            Directory.Delete($@"{_appEnvironment.WebRootPath}\users\{user.Email}", true);
        }

        public async Task<EditUserModel> GetEditUserModel(string id)
        {
            ApplicationUser user = await _userManager.FindByIdAsync(id);

            if (user == null)
                return null;

            return new EditUserModel()
            {
                Id = user.Id,
                Name = user.Name,
                Surname = user.Surname
            };
        }

        public async Task<ResultModel> EditUser(EditUserModel model)
        {
            var result = new ResultModel() { IsSuccess = true };

            ApplicationUser user = await _userManager.FindByIdAsync(model.Id);
            if (user == null)
            {
                result.IsSuccess = false;
                result.Message = ResultMessage.NotFound;
                return result;
            }

            user.Name = model.Name;
            user.Surname = model.Surname;

            _unitOfWork.Complete();

            return result;
        }
    }
}
