﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using WebStore.BLL.Interfaces;
using WebStore.DAL.Interfaces;
using WebStore.Models.Helpers;
using WebStore.Models.Logic;

namespace WebStore.BLL.Services
{
    public class CartService : ICartService
    {
        private IUnitOfWork _unitOfWork;

        public CartService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public List<CartItem> GetCart(HttpContext context)
        {
            return SessionHelper.GetObectFromJson<List<CartItem>>(context.Session, "cart");
        }

        public ResultModel Add(int id, HttpContext context)
        {
            var result = new ResultModel() { IsSuccess = true };

            var productFromDb = _unitOfWork.Products.Get(id);
            if (productFromDb == null)
            {
                result.IsSuccess = false;
                result.Message = ResultMessage.NotFound;
                return result;
            }

            if (SessionHelper.GetObectFromJson<List<CartItem>>(context.Session, "cart") == null)
            {
                List<CartItem> cart = new List<CartItem>();

                cart.Add(
                    new CartItem
                    {
                        Product = productFromDb,
                        Quantity = 1
                    }
                );

                SessionHelper.SetObjectAsJson(context.Session, "cart", cart);
            }
            else
            {
                List<CartItem> cart = SessionHelper.GetObectFromJson<List<CartItem>>(context.Session, "cart");
                int index = GetIndexIfItemExists(cart, id);
                if (index > -1)
                {
                    cart[index].Quantity++;
                }
                else
                {
                    cart.Add(
                        new CartItem
                        {
                            Product = productFromDb,
                            Quantity = 1
                        }
                    );
                }

                SessionHelper.SetObjectAsJson(context.Session, "cart", cart);
            }

            return result;
        }

        public ResultModel Remove(int id, HttpContext context)
        {
            var result = new ResultModel() { IsSuccess = true };

            List<CartItem> cart = SessionHelper.GetObectFromJson<List<CartItem>>(context.Session, "cart");
            int index = GetIndexIfItemExists(cart, id);
            if (index > -1)
            {
                cart.RemoveAt(index);
                SessionHelper.SetObjectAsJson(context.Session, "cart", cart);
            }
            else
            {
                result.IsSuccess = false;
                result.Message = "Item not found";
            }

            return result;
        }

        public ResultModel RemoveSingle(int id, HttpContext context)
        {
            var result = new ResultModel() { IsSuccess = true };

            List<CartItem> cart = SessionHelper.GetObectFromJson<List<CartItem>>(context.Session, "cart");
            int index = GetIndexIfItemExists(cart, id);
            if (index > -1)
            {
                if (cart[index].Quantity <= 1)
                    cart.RemoveAt(index);
                else
                    cart[index].Quantity--;

                SessionHelper.SetObjectAsJson(context.Session, "cart", cart);
            }
            else
            {
                result.IsSuccess = false;
                result.Message = "Item not found";
            }

            return result;
        }

        public void EmptyCart(HttpContext context)
        {
            context.Session.Remove("cart");
        }

        #region Private helpers
        private int GetIndexIfItemExists(List<CartItem> cart, int id)
        {
            for (int i = 0; i < cart.Count; i++)
            {
                if (cart[i].Product.Id.Equals(id))
                    return i;
            }

            return -1;
        }

        public ResultModel AddSingle(int id, HttpContext context)
        {
            var result = new ResultModel() { IsSuccess = true };

            List<CartItem> cart = SessionHelper.GetObectFromJson<List<CartItem>>(context.Session, "cart");
            int index = GetIndexIfItemExists(cart, id);
            if (index > -1)
            {
                cart[index].Quantity++;
                SessionHelper.SetObjectAsJson(context.Session, "cart", cart);
            }
            else
            {
                result.IsSuccess = false;
                result.Message = "Item not found";
            }

            return result;
        }
        #endregion
    }
}
