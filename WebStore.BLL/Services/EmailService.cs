﻿using MimeKit;
using MailKit.Net.Smtp;
using System;
using System.Threading.Tasks;
using WebStore.Models.Interfaces;
using WebStore.BLL.Interfaces;
using WebStore.Models.Logic;

namespace WebStore.BLL.Services
{
    public class EmailService : IEmailService
    {
        private readonly IEmailConfiguration _emailConfiguration;

        public EmailService(IEmailConfiguration emailConfiguration)
        {
            _emailConfiguration = emailConfiguration;
        }

        public async Task SendEmailAsync(EmailContent emailContent)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("morozishko95@gmail.com"));
            emailMessage.To.Add(new MailboxAddress(emailContent.Email));
            emailMessage.Subject = emailContent.Subject;

            var builder = new BodyBuilder();
            builder.HtmlBody = emailContent.Message;
            if (emailContent.FileNames.Count > 0)
            {
                foreach (var fileName in emailContent.FileNames)
                {
                    builder.Attachments.Add(fileName);
                }
            }

            emailMessage.Body = builder.ToMessageBody();

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(_emailConfiguration.SmtpServer, _emailConfiguration.SmtpPort, false);
                await client.AuthenticateAsync(_emailConfiguration.SmtpUsername, _emailConfiguration.SmtpPassword);
                await client.SendAsync(emailMessage);

                await client.DisconnectAsync(true);
            }
        }
    }
}
